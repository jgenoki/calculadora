
public class Calculador {
	private Calcular cal;
	
	public void operaciones(Calcular object){
		this.cal = object;
	}
	
	public void operar(int num_uno, int num_dos){
		this.cal.operar(num_uno, num_dos);
	}
}
